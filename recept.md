# Opportunistische Warmte met Compost

> *Iedereen heeft wel een koude plek in de woning.  Bij mij is dat
> een betonnen trap die op de bodem ligt en naar de woonverdieping
> voert.  Een duurzame oplossing was een zak compost die hem van
> onderaf verwarmt.*

### Ingrediënten:

  * 1 grote polyethyleen zak, rond 1 m3
  * 1/2 m3 herfstbladeren met wat kleine takken
  * 1 el levende aarde
  * 2 statiegeldkratjes als afstandsstukken van de bodem
  * 2-3m lange, stijve steunen (ik gebruikte oude ski's)
  * Een steun op halve traphoogte, bijvoorbeeld een kast
  * 1,5m x 4m isolatiemateriaal met hoge R-waarde
  * 16 ophanghaakjes
  * 4 spanbanden
  * touw ofzo, om het isolatiemateriaal vast te zetten
  * thermometers naar smaak

De voorbereiding voor dit recept kost een uur; de bereiding neemt
ongeveer 20 minuten werk, en loopt dan gedurende de winter met af
en toe roeren.  Het afserveren in de lente kost ook ongeveer
20 minuten.  De maaltijd kostte me €8 aan nieuwe ingrediënten.

### Voorbereiding:

 1. Maak **haakjes aan de traprand** vast.  Deze dragen niet veel gewicht,
    maar duwen het compost tegen de trap zodat de warmte beter wordt
    overgedragen.

 2. Klem de **kratjes onder de trap**; ze worden later vastgehouden door het
    gewicht dat er schuin op gaat drukken.  Je zou ze met isolatiemateriaal
    kunnen opvullen.  Als een zijkant van de trap niet bereikbaar is maak
    daar dan nu alvast haakjes met spanbanden vast.

 3. Zorg voor een **steunpunt halverwege** tot driekwart van de hoogte van de
    trap.  Een hoek van een kast bijvoorbeeld.  Het doel is een laag van
    0,5 meter bladeren te maken (plus dikte isolatiemateriaal).

 4. Zet de **steunen haaks op de kratjes** en tegen het steunpunt, zó dat ze
    het vlak van de trap aan de onderkant volgen.  Leg daaronder (eerst) de
    spanbanden en helemaal buitenaan het isolatiemateriaal en bijvoorbeeld
    touwtjes om dat te bevestigen.

### Bereiding.

> *Dit recept is vooral smakelijk doordat het warmte en zon wint in een tijd
> met voerschot, namelijk de zomer, en de warmte eruit wint in tijden van
> schaarste, namelijk de winter.  Duurzamer kan niet.  Het biedt geen
> zekerheid, maar is meer opportunistisch en vermindert de afhankelijkheid
> van andere warmtevormen die wel zekerheid bieden.  Net als zonnewarmte,
> maar niet met dezelfde pieken en dalen.*

 1. Drapeer de **grote zak** over de steunen.  Als je een thermometer wilt
    uitlezen, bevestig de sensor dan ergens halverwege aan de onderkant
    van de zak, omdat het daar het warmst zal zijn.  Aflezen wil je
    buiten de isolatielaag kunnen doen.

 2. **Haal herfstblad** met kruiwagens of emmers en vul de grote zak daarmee.  Stop
    er af en toe takken tussen.  Eikeblad verteert erg langzaam, vermijd
    dat liever.  Het is prima als het blad vochtig is, maar kletsnat kan
    betekenen dat het proces traag op gang komt.  Je zoekt een laag van
    ongeveer een halve meter dik; je constructie zal aan de zijkanten van
    de trap dunner zijn dus onder een trap van 3m past ongeveer 0,5 m3 blad.

 2. Voeg een **eetlepel levende aarde** toe, met daarin alle microbes die het
    proces nodig heeft.  Als het blad deels van de grond komt heeft het die
    al lang op zich, natuurlijk.  In elk geval zullen de bacterieën zich
    vanzelf verdelen langs het ingerichte bladerenbuffet.

 3. **De zak wil ademen**, al is het maar een beetje.  Er is zuurstof nodig en
    CO₂ moet weg kunnen; dat is wat composteren doet.  Als het blad in elkaar
    zakt ontstaan onder de taken ademwegen voor de compost.  Let ook op dat er
    waterdamp vrijkomt, ook dat moet weg kunnen; als het neerslaat geeft
    het ook warmte af, als je dat kunt opvangen is dat perfect; ik ving het
    onder de dakisolatie tegen het plafond.  De ruimte moet ook zuurstof
    aanvoeren en vocht en CO₂ kunnen afvoeren, dus enige ventilatie is nodig,
    ook van de plek waar vocht neerslaat.  De garage waarin ik het deed had
    kleine ventilatiestroken bij de bodem, en de isolatie had openingen,
    dat volstond voor mij.

 4. Trek de zak met compost **tegen de trap met de spanbanden**, en zorg dat de
    klemmen bereikbaar blijven zodat je dat later kunt herhalen.  De stijve
    steunen verdelen de druk (maar als je te weinig daarvan hebt kan het er
    wel doorheen drukken).

 5. **Drapeer het isolatiemateriaal** er nu omheen.  Dit spiegelt de warmte
    en daardoor kan het compost warm worden.  Je kunt met deze en een paar
    andere parameters spelen om zo het tempo te bepalen.  Je zou zelfs een
    heeeel langzaam reagerende thermostaat kunnen bouwen.

 6. **Onderhoud gedurende de winter** bestaat eruit soms de bladeren tegen de
    trap te trekken met de spanbanden, als het wat sneller moet zou je
    vocht kunnen toevoegen (urine levert stikstof en is lekker warm, dus
    dat werkt goed en stinkt niet als je niet overdrijft) en als je
    constructie het mogelijk maakt zou je het wat kunnen omroeren.

 7. **Bladcompost is heel rustig**, en geeft nauwelijks problemen: rotting
    (en putlucht) wordt voorkomen doordat het luchtig blijft en onderin
    geen vocht verzamelt; ammoniak (pislucht) wordt voorkomen doordat blad
    niet veel stikstof heeft en zelfs extra kan opnemen uit een paar
    urinegiften.  En dieren voelen zich ook niet tot dit materiaal
    aangetrokken.  Goede compost stinkt niet, dus ruik af en toe.  De
    noodzaak voor ventilatie blijft bestaan, want CO₂ ruik je niet.

 8. Composteren gebeurt rond 40⁰C in 2 maanden of rond 70⁰C in 2-3 weken.
    Deze varianten werken met andere soorten microbes.  Wil dit als
    winterwarmte werken, dan moet je **mikken op 40⁰C** waarbij het proces
    geleidelijk de opportunistische warmte levert die andere warmtebronnen
    minder nodig maken.  Ook op deze lagere temperatuur draagt het
    bij aan de warmte, alleen doet het dat rustiger.  Je kunt niet even de
    compost opstoken, maar wel vooruitzien dat er koude komt en dan zou je
    urine als verdunde en voorverwarmde stikstof kunnen inbrengen.   De
    dikte van 0,5m voor het blad is uitgekozen op de doeltemperatuur.
    De hogere temperatuur stopt zichzelf, maar komt wel rond het smeltpunt
    van PVC, vandaar de suggestie van PE voor de grote compostzak.

 8. In de lente is het compostproces nog niet af, dit proces werkt langzaam
    De winteropbrengst is daardoor lager dan de geschatte 1,2 kWh/kg
    die compost aan warmte afgeeft.  Het zit wel vol bacterieën en dat is
    aantrekkelijk voor wormen die daarvan leven en daarbij het materiaal
    in hoogwaardige compost omzetten.  Verwerk het dus vooral als mulch.

Het proces kent veel variaties, en werkt altijd net wat anders.  Ga
het experiment aan en leer.  Dat is ook wat ik nog doe; bijvoorbeeld
heb ik geleerd dat het blad niet onderin moet uitzakken in een bult,
omdat het dan te snel loopt.  En ik heb geleerd dat ik op de trap niet
goed kan meten, omdat de warmte daar uitsmeert, dus dat een thermometer
binnenin beter werkt.  Ik leer en verbeter mijn eigen setup, want het
eerste jaar smaakt naar meer!

**Meer weten over compost?**  Kijk op https://www.compostmagazine.com/compost-science/

**Meer weten over microbes?**  Kijk naar https://media.ccc.de/v/mch2022-155-hacking-with-microbes
en download achterliggende materialen van https://program.mch2022.org/mch2022/talk/D3QKXL/
